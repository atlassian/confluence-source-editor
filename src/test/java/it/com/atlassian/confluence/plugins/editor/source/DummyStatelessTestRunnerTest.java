package it.com.atlassian.confluence.plugins.editor.source;

import com.atlassian.confluence.test.api.model.person.UserWithDetails;
import com.atlassian.confluence.test.rpc.api.ConfluenceRpcClient;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.page.NoOpPage;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

@RunWith(ConfluenceStatelessTestRunner.class)
public class DummyStatelessTestRunnerTest {
    @Inject
    private static ConfluenceRpcClient rpcClient;
    @Inject
    private static Timeouts timeout;
    @Inject
    private static ConfluenceTestedProduct confluenceInstance;

    @Test
    public void testLogin() {
        confluenceInstance.login(UserWithDetails.ADMIN, NoOpPage.class);
    }
}
