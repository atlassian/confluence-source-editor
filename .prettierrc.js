module.exports = {
    trailingComma: 'none', // Needed for es5 script
    tabWidth: 4,
    singleQuote: true,
    printWidth: 120,
    overrides: [
        {
            files: 'package.json',
            options: {
                tabWidth: 2,
            },
        },
    ],
};
