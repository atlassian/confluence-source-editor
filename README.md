# THIS REPOSITORY HAS BEEN FROZEN AND MIGRATED

This repository has been migrated to the confluence-open-plugins repository. This is part of the developer productivity team's efforts in bringing homogenous plugins closer together and allowing for overarching changes to be easier to manage. Please checkout the new repository in order to start working on this plugin.

* New home: [confluence-open-plugins](https://bitbucket.org/atlassian/confluence-open-plugins/src/master/) 
* Project status: [Confluence Monorepo Plugin Status](https://hello.atlassian.net/wiki/spaces/CSD/pages/2786862612/Confluence+Monorepo+plugin+migration+status) 
* Project overview: [Move closer to Confluence DC monorepo and colocation of tests](https://hello.atlassian.net/wiki/spaces/CSD/pages/2641350532/Move+closer+to+Confluence+DC+monorepo+and+colocation+of+tests) 
* Contact for help: Contact person on Project status page or [#conf-dc-thunderbird](https://atlassian.slack.com/archives/C01KJLNDB1D)

# Confluence Source Editor Plugin

The Confluence Source Editor plugin allows users to view and edit the underlying storage format for a Confluence page. The source editor is ideal for:

* Copy/pasting back and forth between Confluence and your preferred desktop editor
* Quickly viewing and editing macro parameters, link URLs, image names and more.
* Quickly fixing editor formatting errors
* Search and replace macro parameters and link urls.

When the plugin is installed and enabled on your Confluence site, users will see the new source editor button <> in the editor toolbar. Features include:

* XML syntax highlighting and line numbers to make the source easy to read.
* Simple validation ensures that your XHTML is well formed before you apply the changes to the page.

Access to the Source Editor may be restricted to specific groups by the system administrator.

Documentation for the Confluence Storage Format is available [here](http://confluence.atlassian.com/display/DOC/Confluence+Storage+Format).

This plugin is available on the [Atlassian Marketplace](https://marketplace.atlassian.com/plugins/com.atlassian.confluence.plugins.editor.confluence-source-editor).